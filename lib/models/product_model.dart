import 'package:bs_admin/helpers/helpers.dart';
import 'package:bs_admin/models/type_model.dart';

class ProductModel {
  int id;
  String? typeid;
  String? productcd;
  String? productnm;
  String? description;

  Map<String, dynamic>? _parent;

  ProductModel(
      {this.id = 0,
      this.typeid,
      this.productcd,
      this.productnm,
      this.description,
      Map<String, dynamic>? parent})
      : _parent = parent;

  factory ProductModel.fromJson(Map<String, dynamic> map) {
    return ProductModel(
      id: parseInt(map['id']),
      typeid: parseString(map['typeid']),
      productcd: parseString(map['productcd']),
      productnm: parseString(map['productnm']),
      description: parseString(map['description']),
      parent: map['parent'],
    );
  }

  TypeModel get parent {
    if (_parent == null) return TypeModel();

    return TypeModel.fromJson(_parent!);
  }
}
