part of datasource;

class ProductText {
  static String title = 'Produk';
  static String subTitle = 'Data produk';

  static String formParent = 'Tipe';
  static String formCode = 'Kode';

  static String tableParentId = 'typeid';
  static String tableCode = 'productcd';
  static String tableName = 'productnm';
}
