import 'package:bs_admin/helpers/helpers.dart';
import 'package:bs_admin/models/product_model.dart';
import 'package:bs_admin/services/product_service.dart';
import 'package:bs_admin/utils/utils.dart';
import 'package:bs_admin/views/components/dialog_confirm.dart';
import 'package:bs_admin/views/masters/products/source/datasource.dart';
import 'package:bs_admin/views/masters/products/widget/product_form.dart';
import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';

abstract class ProductPresenterContract implements ViewContract {}

class ProductPresenter extends ProductFormSource {
  GlobalKey<State> _formState = GlobalKey<State>();

  final ProductPresenterContract viewContract;

  ProductPresenter(this.viewContract);

  ProductService productService = ProductService();
  ProductModel productModel = ProductModel();

  ProductDataSource productSource = ProductDataSource();

  void setLoading(bool loading) {
    isLoading = loading;
    viewContract.updateState();

    if (_formState.currentState != null)
      _formState.currentState!.setState(() {});
  }

  Map<String, String> getDatas() {
    return {
      'typeid': selectParent.getSelectedAsString() != ''
          ? selectParent.getSelectedAsString()
          : '0',
      'productcd': inputCode.text,
      'productnm': inputName.text,
      'description': inputDescription.text,
    };
  }

  void setData(ProductModel product) {
    productModel = product;

    if (productModel.typeid != 0)
      selectParent.setSelected(BsSelectBoxOption(
          value: productModel.typeid,
          text: Text(parseString(productModel.parent.typenm))));

    typeid = parseString(productModel.id);
    inputCode.text = parseString(productModel.productcd);
    inputName.text = parseString(productModel.productnm);
    inputDescription.text = parseString(productModel.description);

    setLoading(false);
  }

  void resetData() {
    typeid = '';
    selectParent.clear();
    inputCode.clear();
    inputName.clear();
    inputDescription.clear();
  }

  Future datatables(BuildContext context, Map<String, String> params) async {
    return await productService.datatables(params).then((value) {
      if (value.result!) {
        productSource.response = BsDatatableResponse.createFromJson(value.data);
        productSource.onEditListener = (typeid) => edit(context, typeid);
        productSource.onDeleteListener = (typeid) => delete(context, typeid);
        setLoading(false);
      }
    });
  }

  void add(BuildContext context) {
    resetData();
    setLoading(false);

    showDialog(
        context: context,
        builder: (context) => ProductFormModal(
              key: _formState,
              presenter: this,
              onSubmit: () => store(context),
            ));
  }

  void store(BuildContext context) {
    setLoading(true);
    productService.store(getDatas()).then((res) {
      setLoading(false);

      if (res.result!) {
        Navigator.pop(context);
        productSource.controller.reload();
      }
    });
  }

  void edit(BuildContext context, int id) {
    resetData();
    setLoading(true);

    showDialog(
      context: context,
      builder: (context) => ProductFormModal(
        key: _formState,
        presenter: this,
        onSubmit: () => update(context, id),
      ),
    );

    productService.show(id).then((res) {
      if (res.result!) {
        setData(ProductModel.fromJson(res.data));
      }
    });
  }

  void update(BuildContext context, int id) {
    setLoading(true);
    productService.update(id, getDatas()).then((res) {
      setLoading(false);

      if (res.result!) {
        Navigator.pop(context);
        productSource.controller.reload();
      }
    });
  }

  void delete(BuildContext context, int id) {
    showDialog(
        context: context,
        builder: (context) => DialogConfirm(
              onPressed: (value) {
                if (value == DialogConfirmOption.YES_OPTION) {
                  productService.delete(id).then((res) {
                    setLoading(false);
                    Navigator.pop(context);
                    productSource.controller.reload();
                  });
                }
              },
            ));
  }
}
